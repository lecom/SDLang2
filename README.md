# SDLang2
PROPER bindings of the SDL2 library for D. Unlike derelict, which is supposed to be installed via dub and which loads the library and dlsyms its contents into function pointer, this project aims to merely provide the D equivalent of C headers, allowing you to link SDL2 the standard way via linker. Oh, and also PERFORMANCE GAIN 8-)

NOTE:
This bindings are totally incomplete, I only added what I myself needed for a certain project.
This bindings contain parts of SDL2 header files.
