module sdl2;

public import sdl2i;

SDL_Surface *SDL_LoadBMP(const char *file){
	return SDL_LoadBMP_RW(SDL_RWFromFile(file, "rb".ptr), 1);
}

int SDL_SaveBMP(SDL_Surface *srfc, const char *file){
	return SDL_SaveBMP_RW(srfc, SDL_RWFromFile(file, "wb+".ptr), 1);
}


T SDL_BUTTON(T)(T X){return (1 << ((X)-1));}
